package br.com.fanor.service;

import java.util.LinkedList;
import java.util.List;

import br.com.fanor.model.Aluno;

/**
 * @author Samantha
 * @category Service Classe que tem a responsabilidade de Fazer todas as
 *           operações de fila
 *
 */
public class FilaService {
	private List<Aluno> alunos = new LinkedList<>();

	/**
	 * @param aluno
	 */
	public void insere(Aluno aluno) {
		alunos.add(aluno);
	}

	/**
	 * @return
	 */
	public Aluno remove() {
		if (alunos.size()==0)
			return null;
		else
			return alunos.remove(0);
	}

	/**
	 * @return
	 */
	public boolean vazia() {
		return alunos.size() == 0;
	}

	public List<Aluno> getAlunos() {
		return alunos;
	}

}

package br.com.fanor.service;

import java.util.LinkedList;
import java.util.List;

import br.com.fanor.model.Peca;

/**
 * @author Samantha
 * @category service
 * Classe de operações de pilha
 *
 */
public class PilhaService {
	private List<Peca> pecas = new LinkedList<Peca>();

	/**
	 * @param peca
	 */
	public void insere(Peca peca) {
		pecas.add(peca);
	}

	/**
	 * @return
	 */
	public Peca remove() {
		if(pecas.size()!=0){
			return pecas.remove(pecas.size() - 1);
		}else
			return null;
	}

	/**
	 * @return
	 */
	public boolean vazia() {
		return pecas.size()==0;
	}

	public List<Peca> getPecas() {
		return pecas;
	}
	
}

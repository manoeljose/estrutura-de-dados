package br.com.fanor.service;

import br.com.fanor.model.No;

public class ArvoreService {

	public void insere(No no, int valor) {
		if (valor < no.getValor()) {
			if (no.getEsquerda() != null) {
				insere(no.getEsquerda(), valor);
			} else {
				no.setEsquerda(new No(valor));
			}
		} else if (valor > no.getValor()) {
			if (no.getDireita() != null) {
				insere(no.getDireita(), valor);
			} else {
				no.setDireita(new No(valor));
			}
		}
	}

	public No remove(No no, int valor) {
		if (no == null) {
			return null;
		}
		if (valor < no.getValor()) {
			no.setEsquerda(remove(no.getEsquerda(), valor));

		} else if (valor > no.getValor()) {
			no.setDireita(remove(no.getDireita(), valor));

		} else if (no.getEsquerda() != null && no.getDireita() != null) // 2
																		// filhos
		{
			no.setValor(encontraAltura(no.getDireita()).getValor());
			no.setDireita(removeAltura(no.getDireita()));
		} else {
			no = (no.getEsquerda() == null) ? no.getEsquerda() : no.getDireita();
		}
		return no;
	}

	public No removeAltura(No node) {
		if (node == null) {
			return null;
		} else if (node.getEsquerda() != null) {
			node.setEsquerda(removeAltura(node.getEsquerda()));
			return node;
		} else {
			return node.getDireita();
		}
	}

	public No encontraAltura(No node) {
		if (node != null) {
			while (node.getEsquerda() != null) {
				node = node.getEsquerda();
			}
		}
		return node;
	}
}

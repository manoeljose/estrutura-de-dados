package br.com.fanor.model;

/**
 * @author Samantha
 * @category model
 * Classe modelo para operações de pilha
 *
 */
public class Peca {

	
	private int id;
	private String nome;

	
	
	/**
	 * @return
	 */
	public String getNome() {
		return nome;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @param nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	
}

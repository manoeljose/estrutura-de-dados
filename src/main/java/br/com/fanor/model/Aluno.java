package br.com.fanor.model;

/**
 * @author Samantha
 * @category Model
 * Classe Modelo para operações nas estruturas
 */
/**
 * @author manoel
 *
 */
public class Aluno {
	
	private int id;

	private String nome;
	
	private String matricula;

	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return
	 */
	public String getMatricula() {
		return matricula;
	}

	/**
	 * @param matricula
	 */
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
	
}

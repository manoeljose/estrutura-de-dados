package br.com.fanor.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/home")
public class HomeController {
	private static final String INICIO_FILA = "fila/inicio";
	private static final String INICIO_PILHA = "pilha/inicio";
	private static final String INICIO_ARVORE_BI = "arvore/inicio";

	@RequestMapping(value = "", method = RequestMethod.GET)
	public String execute(
			@RequestParam(value = "menu", required = false) String menu,
			Model model) {
		switch (menu) {
		case "fila":
			model.addAttribute("menu", menu);
			return INICIO_FILA;
		case "pilha":
			model.addAttribute("menu", menu);
			return INICIO_PILHA;
		case "arv-b":
			model.addAttribute("menu", menu);
			return INICIO_ARVORE_BI;

		default:
			return null;
		}
	}
}

package br.com.fanor.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.com.fanor.model.No;
import br.com.fanor.service.ArvoreService;

@Controller
public class ArvoreBiController {

	private ArvoreService service = new ArvoreService();
	private No raiz = new No(20);

	private static final String INICIO = "arvore/inicio";
	private static final String REMOVIDO = "arvore/remover-no";
	private static final String REMOVER_ALTURA = "arvore/remover-altura";
	private static final String ENCONTRAR_ALTURA = "arvore/encontrar-altura";
	private static final String FORMULARIO = "arvore/formulario";

	@RequestMapping("/arvore-bi")
	public String inicio() {
		return INICIO;
	}

	@RequestMapping("/arvore-bi/formulario")
	public String form() {
		return FORMULARIO;
	}
	
	@RequestMapping("/arvore-bi/remover-no")
	public String formRemoverNo() {
		return REMOVIDO;
	}

	/**
	 * @param peca
	 *            ,model
	 * @return Pagina de Adicionado
	 */
	@RequestMapping(value = "/arvore-bi/inserir", method = RequestMethod.POST)
	public String inserir(
			@RequestParam(value = "valor", required = true) int valor,
			Model model) {
		service.insere(raiz, valor);
		model.addAttribute("no", "adicionado");
		return FORMULARIO;
	}

	/**
	 * @param model
	 * @return Pagina de Removido
	 */
	@RequestMapping(value="/arvore-bi/remover",method=RequestMethod.POST)
	public String remover(
			@RequestParam(value = "valor", required = true) int valor,
			Model model) {
		No removido = service.remove(raiz, valor);
		if (removido == null)
			model.addAttribute("falha", true);
		else
			model.addAttribute("no", removido);
		return REMOVIDO;
	}

	@RequestMapping(value="/arvore-bi/remover-altura",method=RequestMethod.GET)
	public String removerAltura(Model model) {
		No removido = service.removeAltura(raiz);
		if (removido == null)
			model.addAttribute("falha", true);
		else
			model.addAttribute("no", removido);
		return REMOVER_ALTURA;
	}

	@RequestMapping("/arvore-bi/encontra-altura")
	public String encontraAltura(Model model) {
		No altura = service.encontraAltura(raiz);
		if (altura == null)
			model.addAttribute("falha", true);
		else
			model.addAttribute("no", altura);
		return ENCONTRAR_ALTURA;
	}
}

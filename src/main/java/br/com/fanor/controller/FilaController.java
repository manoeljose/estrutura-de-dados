package br.com.fanor.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.fanor.model.Aluno;
import br.com.fanor.service.FilaService;

/**
 * @author Samantha
 * @category Controller Classe que controla todas as chamadas dos menus de fila
 */
@Controller
public class FilaController {

	private FilaService service = new FilaService();

	private static final String INICIO = "fila/inicio";
	private static final String REMOVIDO = "fila/removido";
	private static final String FORMULARIO = "fila/formulario";
	private static final String LISTAGEM = "fila/lista";

	/**
	 * @return Pagina inicial Fila
	 */
	@RequestMapping("/fila")
	public String inicio() {
		return INICIO;
	}

	/**
	 * @return Pagina de formulario
	 */
	@RequestMapping("/fila/formulario")
	public String form() {
		return FORMULARIO;
	}

	/**
	 * @param aluno
	 * @return Pagina de Adicionado
	 */
	@RequestMapping("/fila/inserir")
	public String inserir(Aluno aluno, Model model) {
		service.insere(aluno);
		model.addAttribute("aluno", "adicionado");
		return FORMULARIO;
	}

	/**
	 * @return Pagina de Removido
	 */
	@RequestMapping("/fila/remover")
	public String remover(Model model) {
		Aluno removido = service.remove();
		if (removido == null) {
			model.addAttribute("falha", true);
		} else
			model.addAttribute("aluno", removido);
		return REMOVIDO;
	}

	/**
	 * @param model
	 * @return Pagina de vazia ou não
	 */
	@RequestMapping("/fila/is-vazia")
	public String isVazia(Model model) {
		model.addAttribute("fila", service.vazia());
		model.addAttribute("vazia", true);
		return INICIO;
	}

	@RequestMapping("/fila/lista")
	public String lista(Model model) {
		model.addAttribute("alunos", service.getAlunos());
		return LISTAGEM;
	}

}

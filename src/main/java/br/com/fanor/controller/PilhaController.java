package br.com.fanor.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import br.com.fanor.model.Peca;
import br.com.fanor.service.PilhaService;

/**
 * @author Samantha
 * @category controller Classe para controlar operações de pilha
 *
 */
@Controller
public class PilhaController {

	private PilhaService service = new PilhaService();

	private static final String INICIO = "pilha/inicio";
	private static final String REMOVIDO = "pilha/removido";
	private static final String FORMULARIO = "pilha/formulario";
	private static final String LISTA = "pilha/lista";

	@RequestMapping("/pilha")
	public String inicio() {
		return INICIO;
	}

	@RequestMapping("/pilha/formulario")
	public String form() {
		return FORMULARIO;
	}

	/**
	 * @param peca
	 *            ,model
	 * @return Pagina de Adicionado
	 */
	@RequestMapping("/pilha/inserir")
	public String inserir(Peca peca, Model model) {
		service.insere(peca);
		model.addAttribute("peca", "adicionado");
		return FORMULARIO;
	}

	/**
	 * @param model
	 * @return Pagina de Removido
	 */
	@RequestMapping("/pilha/remover")
	public String remover(Model model) {
		Peca removido = service.remove();
		if (removido == null)
			model.addAttribute("falha", true);
		else
			model.addAttribute("peca", removido);
		return REMOVIDO;
	}

	/**
	 * @param model
	 * @return Pagina de vazia ou não
	 */
	@RequestMapping("/pilha/is-vazia")
	public String isVazia(Model model) {
		model.addAttribute("fila", service.vazia());
		model.addAttribute("vazia", true);
		return INICIO;
	}
	
	@RequestMapping("/pilha/listar")
	public String lista(Model model){
		model.addAttribute("pecas",service.getPecas());
		return LISTA;
	}
	
}

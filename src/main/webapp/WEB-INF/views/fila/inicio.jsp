<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<body>
	<c:choose>
		<c:when test="${not fila and vazia}">
			<div class="alert alert-success">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>H� Alunos Na Fila!</strong>
			</div>
		</c:when>
		<c:when test="${fila and vazia}">
			<div class="alert alert-danger">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>N�o H� Alunos Na Fila!</strong>
			</div>
		</c:when>
		<c:otherwise>

		</c:otherwise>
	</c:choose>

	<div class="jumbotron">
		<h1>Fila</h1>
		<p>No dia a dia, estamos acostumados com as filas em diversos
			lugares: nos bancos, nos mercados, nos hospitais, nos cinemas entre
			outros. As filas s�o importantes pois elas determinam a ordem de
			atendimento das pessoas.</p>
		<p>As pessoas s�o atendidas conforme a posi��o delas na fila. O
			pr�ximo a ser atendido � o primeiro da fila. Quando o primeiro da
			fila � chamado para ser atendido a fila "anda", ou seja, o segundo
			passa a ser o primeiro, o terceiro passa a ser o segundo e assim por
			diante at� a �ltima pessoa.</p>

		<p>Normalmente, para entrar em uma fila, uma pessoa deve se
			colocar na �ltima posi��o, ou seja, no fim da fila. Desta forma, quem
			chega primeiro tem prioridade.</p>

		<p>Assim como Listas e Pilhas, as Filas s�o estruturas de dados
			que armazenam os elementos de maneira sequencial.</p>

		<p>Assim como as Pilhas, as Filas t�m opera��es mais restritas do
			que as opera��es das Listas. Nas Filas, os elementos s�o adicionados
			na �ltima posi��o e removidos da primeira posi��o. Nas Listas, os
			elementos s�o adicionados e removidos de qualquer posi��o</p>
	</div>
</body>
</html>
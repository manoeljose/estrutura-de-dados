<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>

<body>
	<c:choose>
		<c:when test="${alunos.size()==0 }">
			<div class="alert alert-danger">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>N�o H� Alunos Na Fila!</strong>
			</div>
		</c:when>
		<c:otherwise>
			<table class="table table-bordered table-hover table-condensed">
				<thead>
					<tr>
						<th>ID</th>
						<th>Nome</th>
						<th>Matricula</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${alunos }" var="aluno">
						<tr>
							<td>${aluno.id }</td>
							<td>${aluno.nome }</td>
							<td>${aluno.matricula }</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:otherwise>
	</c:choose>
</body>
</html>
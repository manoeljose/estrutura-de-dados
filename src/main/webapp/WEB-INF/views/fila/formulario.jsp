<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>

<body>
	<c:if test="${aluno eq 'adicionado' }">
		<div class="alert alert-success">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Sucesso!</strong> Aluno Adicionado.
		</div>
	</c:if>
	<h3 class="text-primary text-center">Adicione um Aluno na Fila</h3>
	<div class="well well-lg">
		<form role="form" action="inserir" method="post">
			<div class="form-group">
				<label for="nome">Nome: </label><input class="form-control"
					name="nome" id="nome" type="text" />
			</div>
			<div class="form-group">
				<label for="matricula">Matricula:</label><input class="form-control"
					name="matricula" id="matricula" type="text" />
			</div>
			<button type="submit" class="btn btn-primary">Adicionar</button>
		</form>
	</div>
</body>
</html>
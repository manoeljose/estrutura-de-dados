<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<body>

	<div class="jumbotron">
		<c:choose>
			<c:when test="${falha}">
				<h1>N�o h� Pe�as na Pilha</h1>
			</c:when>
			<c:otherwise>
				<h1>A Pe�a ${peca.nome }</h1>
				<h2>Foi Removida</h2>
			</c:otherwise>
		</c:choose>
	</div>
</body>
</html>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix='fn'%>
<body>
	<c:choose>
		<c:when test="${not fila and vazia}">
			<div class="alert alert-success">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>H� Pe�as Na Fila!</strong>
			</div>
		</c:when>
		<c:when test="${fila and vazia}">
			<div class="alert alert-danger">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>N�o H� Pe�as Na Fila!</strong>
			</div>
		</c:when>
		<c:otherwise>

		</c:otherwise>
	</c:choose>

	<div class="jumbotron">
		<h1>Pilha</h1>
		<p>Um determinado produto � composto por diversas pe�as (digamos
			p1, p2, ...,pn). O processo de montagem deste produto � autom�tico
			(executado por uma m�quina) e exige que as pe�as sejam colocadas em
			uma ordem espec�fica (primeiro a p1, depois a p2, depois a p3 e assim
			por diante). As pe�as s�o empilhadas na ordem adequada e a m�quina de
			montagem vai retirando pe�a por pe�a do topo desta pilha para poder
			montar o produto final.</p>
		<p>A mesma m�quina que faz a montagem � capaz de trocar uma pe�a
			quebrada de um produto j� montado. O que a m�quina faz � desmontar o
			produto at� chegar na pe�a defeituosa, troc�-la e ent�o depois
			recolocar as pe�as que foram retiradas. Isso tamb�m � feito com o uso
			da pilha de pe�as. Veja a seguir o algoritmo que a m�quina montadora
			implementa para fazer a manuten��o de um produto com defeito.</p>
	</div>
</body>
</html>
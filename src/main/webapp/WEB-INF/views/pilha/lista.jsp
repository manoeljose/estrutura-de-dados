<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>

<body>
	<c:choose>
		<c:when test="${pecas.size()==0 }">
			<div class="alert alert-danger">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>N�o H� Pe�as Na Pilha!</strong>
			</div>
		</c:when>
		<c:otherwise>
			<table class="table table-bordered table-hover table-condensed">
				<thead>
					<tr>
						<th>ID</th>
						<th>pe�a</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${pecas }" var="peca">
						<tr>
							<td>${peca.id }</td>
							<td>${peca.nome }</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:otherwise>
	</c:choose>
</body>
</html>
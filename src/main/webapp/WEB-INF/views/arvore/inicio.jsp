<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<body>
	<c:choose>
		<c:when test="${not fila and vazia}">
			<div class="alert alert-success">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>H� N�s Na Arvore!</strong>
			</div>
		</c:when>
		<c:when test="${fila and vazia}">
			<div class="alert alert-danger">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>N�o H� N�s Na Arvore!</strong>
			</div>
		</c:when>
		<c:otherwise>

		</c:otherwise>
	</c:choose>

	<div class="jumbotron">
		<h1>Arvores</h1>
	</div>
</body>
</html>
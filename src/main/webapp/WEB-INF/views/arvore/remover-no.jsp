<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>

<body>
	
	<c:choose>
		<c:when test="${not falha and no!=null}">
				<div class="alert alert-success">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Sucesso!</strong> No de valor ${no.valor } Removido.
		</div>
		</c:when>
		<c:otherwise>
			<div class="alert alert-danger">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Nó Não encontrado</strong>
			</div>
		</c:otherwise>
	</c:choose>
	
	<h3 class="text-primary text-center">Remova um No na Arvore</h3>
	<div class="well well-lg">
		<form role="form" action="remover" method="post">
			<div class="form-group">
				<label for="valor">Valor:</label><input class="form-control"
					name="valor" id="valor" type="text" />
			</div>
			<button type="submit" class="btn btn-primary">Remover</button>
		</form>
	</div>
</body>
</html>
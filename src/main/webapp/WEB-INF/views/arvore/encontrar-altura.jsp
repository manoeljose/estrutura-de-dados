<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix='fn'%>
<body>

	<div class="jumbotron">
		<c:choose>
			<c:when test="${falha}">
				<h1>N�o h� N�s na Arvore</h1>
			</c:when>
			<c:otherwise>
				<h1>N� ${no.valor }</h1>
				<h2>Foi Encontrado</h2>
			</c:otherwise>
		</c:choose>
	</div>
</body>
</html>
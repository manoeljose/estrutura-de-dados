<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>

<body>
	<c:if test="${no eq 'adicionado' }">
		<div class="alert alert-success">
			<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<strong>Sucesso!</strong> No Adicionado.
		</div>
	</c:if>
	<h3 class="text-primary text-center">Adicione um No na Arvore</h3>
	<div class="well well-lg">
		<form role="form" action="inserir" method="post">
			<div class="form-group">
				<label for="valor">Valor:</label><input class="form-control"
					name="valor" id="valor" type="text" />
			</div>
			<button type="submit" class="btn btn-primary">Adicionar</button>
		</form>
	</div>
</body>
</html>
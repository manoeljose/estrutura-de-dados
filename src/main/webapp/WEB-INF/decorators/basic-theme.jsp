<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="pt-br">
<c:set value="${pageContext.request.contextPath}/resources" var="assets" />
<c:set value="${pageContext.request.contextPath}/" var="path" />
<c:set value="menus" var="menus" />
<c:set value="${pageContext.request.contextPath}/home" var="home" />
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Trabalho Estrutura de dados</title>
<meta name="generator" content="Bootply" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="${assets}/css/bootstrap.min.css" rel="stylesheet">
<!--[if lt IE 9]>
			<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
<link href="${assets}/css/styles.css" rel="stylesheet">
</head>
<body>
	<div id="header" class="navbar navbar-default navbar-fixed-top">
		<div class="navbar-header">
			<button class="navbar-toggle collapsed" type="button"
				data-toggle="collapse" data-target=".navbar-collapse">
				<i class="icon-reorder"></i>
			</button>
			<a class="navbar-brand" href="${path}welcome"> Bem Vindo! </a>
		</div>
		<nav class="collapse navbar-collapse">
			<ul class="nav navbar-nav">
				<li><a href="${home}?menu=pilha">Pilha</a></li>
				<li><a href="${home}?menu=fila">Fila</a></li>
				<li class="dropdown"><a href="#" class="dropdown-toggle"
					data-toggle="dropdown">Arvores<b class="caret"></b></a>
					<ul class="dropdown-menu">
						<li><a href="${home}?menu=arv-b">Arvore Bin�ria</a></li>
						<li><a href="${home}?menu=arv-avl">Arvore AVL</a></li>
					</ul></li>
			</ul>
		</nav>
	</div>
	<c:if test="${menu !=null }">
		<div id="sidebar-wrapper" class="col-md-1">
			<div id="sidebar">
				<c:choose>
					<c:when test="${menu eq'arv-b' }">
						<jsp:include page="${menus }/menu-arv-b.jsp" />
					</c:when>
					<c:when test="${menu=='arv-avl' }">
						<jsp:include page="${menus }/menu-arvore-avl.jsp" />
					</c:when>
					<c:when test="${menu eq'pilha'}">
						<jsp:include page="${menus }/menu-pilha.jsp" />
					</c:when>
					<c:when test="${menu eq 'fila'}">
						<jsp:include page="${menus}/menu-fila.jsp" />
					</c:when>
					<c:otherwise>
						<ul class="nav list-group">
						</ul>
					</c:otherwise>
				</c:choose>
			</div>
		</div>
	</c:if>
	<c:choose>
		<c:when test="${menu==null }">
			<div id="main-wrapper" class="col-md-12 ">
				<div id="main">
					<div class="page-header">
						<div class="container center-block">
							<decorator:body />
							<footer class="footer">
								<div class="container text-center">
									<p>Trabalho Estrutura de Dados</p>
									<p>Aluna Samantha</p>
								</div>
							</footer>
						</div>
					</div>
				</div>
			</div>
		</c:when>
		<c:otherwise>
			<div id="main-wrapper" class="col-md-11 pull-right">
				<div id="main">
					<div class="page-header">
						<div class="container center-block">
							<decorator:body />
							<footer class="footer">
								<div class="container text-center">
									<p>Trabalho Estrutura de Dados</p>
									<p>Aluna Samantha</p>
								</div>
							</footer>
						</div>
					</div>
				</div>
			</div>
		</c:otherwise>
	</c:choose>
	<!-- script references -->
	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script src="${assets}/js/bootstrap.min.js"></script>
</body>
</html>